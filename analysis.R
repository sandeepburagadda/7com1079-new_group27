library(tidyverse)
data<- read.csv("heart.csv")
get_age <- function(age) {
  if (age >= 29 && age < 39) {
    return("29 to 39")
  } else if (age >= 39 && age < 49) {
    return("39 to 49")
  } else if (age >= 49 && age < 59) {
    return("49 to 59")
  } else if (age >= 59 && age < 69) {
    return("59 to 69")
  } else if (age >= 69 && age < 79) {
    return("69 to 79")
  }
}

data$Age_Interval = mapply(get_age,data$age)
df29<-filter(data, Age_Interval== "29 to 39")
df39<-filter(data, Age_Interval== "39 to 49")
df49<-filter(data, Age_Interval== "49 to 59")
df59<-filter(data, Age_Interval== "59 to 69")
df69<-filter(data, Age_Interval== "69 to 79")
head(df29)
t.test(df29$chol, df39$chol)
t.test(df29$chol, df49$chol)
t.test(df29$chol, df59$chol)
t.test(df29$chol, df69$chol)
t.test(df39$chol, df49$chol)
t.test(df39$chol, df59$chol)
t.test(df39$chol, df69$chol)
t.test(df49$chol, df59$chol)
t.test(df49$chol, df69$chol)
t.test(df59$chol, df69$chol)